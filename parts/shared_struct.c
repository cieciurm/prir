#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#define KEY 777
#define BUFFOR_SIZE 80
#define MAX_SIZE 100

int current_child; /* ktore dziecko jest aktualnie obslugiwane */
int flag = 0; /* zostal wyslany sygnal */

typedef 
struct s {
	double sums[MAX_SIZE];
	int* ranges;
} data_t;

int
get_range(int mem_id, int n) {
	data_t *adr;
	int range;

	adr = shmat(mem_id, 0, 0);

	if (adr == (data_t *)-1) {
		printf("Blad shmat!\n");
		exit(2);
	}

	range = adr->ranges[n];

	/*printf("Rendzyk=%d\n", range);*/
	return range;
}

double*
get_part_vector(char* filename, int mem_id) {
	int begin, end;
	double *part_vector;
	char buffor[BUFFOR_SIZE+1];
	int n, vector_size, current_line;
	FILE *f;

	/* zwraca koniec przedzialu - prawostronnie otwarty */
	end = get_range(mem_id, current_child) - 1;

	if (current_child == 0) { /* pierwszy element */
		begin = 0;
	}
	else {
		begin = get_range(mem_id, current_child - 1);
	}

	vector_size = end - begin + 1;

	/*printf("Zakresik: [%d;%d]\n", begin, end);*/

	f = fopen(filename, "r");
	
	if (f == NULL) {
		printf("Blad podczas otwierania pliku\n");
		exit(7);
	}

	part_vector = malloc(sizeof(double) * vector_size + 1);
	part_vector[0] = vector_size;

	/* linia z liczba elementow */
	fgets(buffor, BUFFOR_SIZE, f);
	/* linia na ktorej jestesmy, liczac od 0 */
	current_line = 0;
	n = 1;

	while (fgets(buffor, BUFFOR_SIZE, f) != NULL) {
		if (current_line >= begin && current_line <= end) {
			part_vector[n] = atof(buffor);
			n++;
		}
		current_line++;
		if (current_line > end)
			break;
	}

	/*printf("rozmiar: %d\n", vector_size);*/

	fclose(f);
	return part_vector;
} 

void
on_usr1(int signal) {
	/*printf("Otrzymałem USR1, pid=%d, signal=%d\n", getpid(), signal);*/
	printf("Otrzymałem USR1, i=%d\n", current_child);
	flag = 1;
}

void
print_double_array(double *t, int size) {
	int i;

	/* 
	 * wypisywanie tablicy od 2-giego elementu
	 * bo w w 1-szym mieszka rozmiar
	*/
	for (i = 1; i <= size; i++)
		printf("[%g]", t[i]);
	printf("\n");
}

double
sum_vector(double *t) {
	int i;
	double s;
	s = 0;

	/* 
	 * sumowanie tablicy od 2-giego elementu
	 * bo w w 1-szym mieszka rozmiar
	*/
	for (i = 1; i <= t[0]; i++)
		s += t[i];

	return s;
}

void
assign_usr1_handler() {
	sigset_t mask; 
	sigemptyset(&mask); 
	struct sigaction usr1;
	usr1.sa_handler = (&on_usr1);
	usr1.sa_mask = mask;
	usr1.sa_flags = SA_SIGINFO;
	sigaction(SIGUSR1, &usr1, NULL);
	sigprocmask(SIG_BLOCK, &mask, NULL);
}

int* 
create_ranges_array(int n, int no_of_children) {
	int i, tmp, remainder, step;
	int *ranges, *steps;

	ranges = malloc(no_of_children * sizeof(int));
	steps = malloc(no_of_children * sizeof(int));

	step = n / no_of_children;
	tmp = 0;

	for (i = 0; i < no_of_children; i++) {
		steps[i] = step;
	}

	remainder = n % no_of_children;

	while (remainder > 0) {
		for (i = 0; i < no_of_children && remainder > 0; i++) {
			steps[i]++;
			remainder--;
		}
	}

	for (i = 0; i < no_of_children; i++) {
		tmp += steps[i];
		ranges[i] = tmp;
	}

	free(steps);

	return ranges;
}

int
main(int argc, char** argv) {
	key_t key;
	int mem_id, i, n_children, finished_process, vector_size;
	int *pids;
	double total_sum;
	double *vector_part;
	data_t *adr, *adr2, *adr3, *d;
	pid_t pid;
	char* filename;
	FILE *f;
	char buffor[BUFFOR_SIZE];

	if (argc > 2) {
		n_children = atoi(argv[1]);
		filename = argv[2];
	} else {
		exit(-1);
	}

	f = fopen(filename, "r");
	fgets(buffor, BUFFOR_SIZE, f);
	fclose(f);

	vector_size = atoi(buffor);

	/*key = 1234; *//* 0x4d2 */
	key = KEY; /* przestalo dzialac na kluczu 1234 :O */

	mem_id = shmget(key, sizeof(data_t *), IPC_CREAT|0666);
	if (mem_id == -1) {
		printf("Blad shmget!\n");
		exit(1);
	}

	/*printf("|---------------\n");*/
	/*printf("| mem_id = %d\n", mem_id);*/
	/*printf("|---------------\n\n");*/

	adr = shmat(mem_id, 0, 0);
	if (adr == (data_t *)-1) {
		printf("Blad shmat!\n");
		exit(2);
	}

	d = malloc(sizeof(data_t));
	/*d->sums = malloc(n_children * sizeof(double));*/
	d->ranges = create_ranges_array(vector_size, n_children);

	d->sums[0] = 5;
	d->sums[1] = 5;
	d->sums[2] = 5;

	for (i = 0; i < n_children; i++) 
		printf("[%d]", d->ranges[i]);
	printf("\n");

	memcpy(adr, d, sizeof(data_t));

	/* przypinamy obsluge sygnalu */
	assign_usr1_handler();

	pids = malloc(n_children * sizeof(int));

	for (i = 0; i < n_children; i++) {
		pid = fork();

		if (pid == 0) { /* proces potomny */
			adr2 = shmat(mem_id, 0, 0);

			if (adr2 == (data_t *)-1) {
				printf("Blad shmat!\n");
				exit(2);
			}

			current_child = i;
			/* czekam na sygnał */
			while (flag == 0)
				pause();

			vector_part = get_part_vector(filename, mem_id);
			print_double_array(vector_part, vector_part[0]);
			printf("Suma wektorka = %g\n", sum_vector(vector_part));
			adr2->sums[i] = sum_vector(vector_part);

			for (i = 0; i < n_children; i++)
				printf("[%g]", adr2->sums[i]);
			printf("\n");

			return 0;
		} else {
			pids[i] = pid;
		}

	}

	for (i = 0; i < n_children; i++) {
		sleep(1);
		kill(pids[i], SIGUSR1);
	}
	/*printf("[%d]", pids[i]);*/
	/*printf("\n");*/

	/* czekamy az skoncza sie procesy potomne */
	for (i = 0; i < n_children; i++) {
		finished_process = wait(0);
		if (finished_process == -1)
			exit(5);
		/*printf("* Proces ktory sie zakonczyl PID=%d\n", finished_process);*/
	}

	adr = shmat(mem_id, 0, 0);
	if (adr == (data_t *)-1) {
		printf("Blad shmat!\n");
		exit(2);
	}

	for (i = 0; i < n_children; i++)
		printf("[%g]", adr->sums[i]);
	printf("\n");

	total_sum = 0;

	for (i = 0; i < n_children; i++)
		total_sum += adr->sums[i];

	printf("Mega suma=%g\n", total_sum);

	if (shmctl(mem_id, IPC_RMID, NULL) != 0) {
		printf("Blad przy zwalnianiu pamieci!\n");
		int error = errno;
		printf("errno: %d\n", error);
		printf("error: %s\n", strerror(error));
		exit(-2);
	}

	return 0;
}
