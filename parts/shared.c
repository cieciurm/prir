#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>

int
main() {
	key_t key;
	int mem;
	int* adr;

	key = 1234; /* 0x4d2 */

	mem = shmget(key, 8, IPC_CREAT|0666);
	if (mem == -1) {
		printf("Blad shmget!\n");
		exit(1);
	}

	printf("mem = %d\n", mem);
	

	adr = shmat(mem, 0, 0);
	if (adr == (int*)-1) {
		printf("Blad shmat!\n");
		exit(2);
	}

	/*printf("Wkladam zakresy\n");*/
	/*memcpy(radr, ranges, no_of_children*sizeof(int));*/

	if (shmctl(mem, IPC_RMID, NULL) != 0)
		printf("Blad przy zwalnianiu pamieci!\n");
	return 0;
}
