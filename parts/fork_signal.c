#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <time.h>

#define BUFFOR_SIZE 80
#define MAX_CHILDREN 80

typedef struct s {
	double* sums;
	int* ranges;
} data_t;

void on_usr1(int signal) {
	printf("Otrzymałem USR1, pid=%d\n", getpid());
	sleep(3);
	/*usr1_flag = 1;*/
}

void assign_usr1_handler() {
	sigset_t mask; /* Maska sygnałów */
	sigemptyset(&mask); /* Wyczyść maskę */
	struct sigaction usr1;
	usr1.sa_handler = (&on_usr1);
	usr1.sa_mask = mask;
	usr1.sa_flags = SA_SIGINFO;
	sigaction(SIGUSR1, &usr1, NULL);
	sigprocmask(SIG_BLOCK, &mask, NULL); /* Ustaw maskę dla całego procesu */
}

int* create_ranges(char* filename, int no_children) {
	FILE* f = fopen(filename, "r");
	char buffor[BUFFOR_SIZE+1];
	fgets(buffor, BUFFOR_SIZE, f);
 	int n = atoi(buffor);
	int i;

	int* ranges = malloc(2 * no_children * sizeof(int));

	int step = n / no_children;
	int rest = n % no_children;

	ranges[0] = 0;
	ranges[1] = step;

	for (i = 2; i < n; i += 2) {
		ranges[i] = ranges[i - 1] + 1;
		ranges[i + 1] = ranges[i] + step;
	}

	ranges[2 * no_children + 1] += rest;

	return ranges;
}

void print_array(int* t, int n) {
	int i;
	for (i = 0; i < n; i++)
		printf("[%d]", t[i]);
	printf("\n");
}

int main(int argc, char** argv) {
	int i, no_children;
	pid_t pid;
	pid_t* pids;
	key_t key = 666;
	char* filename = "vector.dat";

	if (argc > 1)
		no_children = atoi(argv[1]);

	printf("Liczba dzieci=%d\n", no_children);

	pids = malloc(no_children * sizeof(int));

	assign_usr1_handler();

	for (i = 0; i < no_children; i++)
		switch (pid = fork()) {
			case -1: /* błąd */
				printf("Error in fork\n");
			case 0: /* proces potomny */
				/* czekamy na sygnal */
				pause();
				return 0;
			default: /* proces macierzysty */
				pids[i] = pid;
		}


	for (i = 0; i < no_children; i++) {
		kill(pids[i], SIGUSR1);
		if (i == 0)
			wait(0);
		printf("Wysylam sygnal SIGUSR1 do procesu o PID=%d\n", pids[i]);
	}

	/*print_array(pids, no_children);*/

	printf("Polecialem dalej bo mam to w ...\n");

	return 0;
}
