#include <stdio.h>
#include <stdlib.h>

int* 
create_ranges_array(int n, int no_of_children) {
	int i, tmp, remainder, step;
	int *ranges, *steps;

	ranges = malloc(no_of_children * sizeof(int));
	steps = malloc(no_of_children * sizeof(int));

	step = n / no_of_children;
	tmp = 0;

	for (i = 0; i < no_of_children; i++) {
		steps[i] = step;
	}

	remainder = n % no_of_children;

	while (remainder > 0) {
		for (i = 0; i < no_of_children && remainder > 0; i++) {
			steps[i]++;
			remainder--;
		}
	}

	for (i = 0; i < no_of_children; i++) {
		tmp += steps[i];
		ranges[i] = tmp;
	}

	free(steps);

	return ranges;
}

void 
print_array(int* t, int n) {
	int i;
	for (i = 0; i < n; i++)
		printf("[%d]", t[i]);
	printf("\n");
}

int
main(int argc, char** argv) {
	int* r;
	int n, c;

	if (argc > 2) {
		n = atoi(argv[1]);
		c = atoi(argv[2]);
	} else {
		return 1;
	}
	r = create_ranges_array(n, c);
	print_array(r, c);

	return 0;
}
