#include <stdio.h>
#include <stdlib.h>

typedef struct s {
	double* sums;
	int* ranges;
} data_t;

void 
print_int_array(int* t, int n) {
	int i;
	for (i = 0; i < n; i++)
		printf("[%d]", t[i]);
	printf("\n");
}

void 
print_double_array(double* t, int n) {
	int i;
	for (i = 0; i < n; i++)
		printf("[%g]", t[i]);
	printf("\n");
}

int
main(int argc, char** argv) {
	data_t* d;
	int n_ranges, n_sums;

	if (argc > 2) {
		n_ranges = atoi(argv[1]);
		n_sums = atoi(argv[2]);
	} else {
		return 1;
	}

	d = malloc(sizeof(data_t));

	d->sums = malloc(n_sums * sizeof(double));
	d->ranges = malloc(n_ranges * sizeof(int));

	print_int_array(d->ranges, n_ranges);
	print_double_array(d->sums, n_sums);

	return 0;
}
