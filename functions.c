#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>

#include "functions.h"

extern int flag;

int
get_range(int mem_id, int n) {
	data_t *adr;
	int range;

	adr = shmat(mem_id, 0, 0);

	if (adr == (data_t *)-1) {
		printf("Blad shmat!\n");
		exit(2);
	}

	range = adr->ranges[n];

	return range;
}

void
on_usr1(int signal) {
	flag = 1;
}

void
print_double_array(double *t, int size) {
	int i;

	/* 
	 * wypisywanie tablicy od 2-giego elementu
	 * bo w w 1-szym mieszka rozmiar
	*/
	for (i = 1; i <= size; i++)
		printf("[%g]", t[i]);
	printf("\n");
}

double
sum_vector(double *t) {
	int i;
	double s;
	s = 0;

	/* 
	 * sumowanie tablicy od 2-giego elementu
	 * bo w w 1-szym mieszka rozmiar
	*/
	for (i = 1; i <= t[0]; i++)
		s += t[i];

	return s;
}

void
assign_usr1_handler() {
	sigset_t mask; 
	sigemptyset(&mask); 
	struct sigaction usr1;
	usr1.sa_handler = (&on_usr1);
	usr1.sa_mask = mask;
	usr1.sa_flags = SA_SIGINFO;
	sigaction(SIGUSR1, &usr1, NULL);
	sigprocmask(SIG_BLOCK, &mask, NULL);
}

int* 
create_ranges_array(int n, int no_of_children) {
	int i, tmp, remainder, step;
	int *ranges, *steps;

	ranges = malloc(no_of_children * sizeof(int));
	steps = malloc(no_of_children * sizeof(int));

	step = n / no_of_children;
	tmp = 0;

	for (i = 0; i < no_of_children; i++) {
		steps[i] = step;
	}

	remainder = n % no_of_children;

	while (remainder > 0) {
		for (i = 0; i < no_of_children && remainder > 0; i++) {
			steps[i]++;
			remainder--;
		}
	}

	for (i = 0; i < no_of_children; i++) {
		tmp += steps[i];
		ranges[i] = tmp;
	}

	free(steps);

	return ranges;
}

