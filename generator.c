#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int
main(int argc, char** argv) {
	int i, s, e, n;
	double d;

	if (argc > 3) {
		n = atoi(argv[1]);
		s = atof(argv[2]);
		e = atof(argv[3]);
	} else {
		n = 10;
		s = 10;
		e = 20;
	}

	srand(time(NULL));

	printf("%d\n", n);
	for (i = 0; i < n; i++) {
		d = (double)rand() * (e-s) / RAND_MAX + s;
		printf("%g\n", d);
	}
	
	return 0;
}
