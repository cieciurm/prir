#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#define ID 1234
#define BUFFOR_SIZE 80
#define MAX_SIZE 100

int flag; /* zostal wyslany sygnal */

typedef 
struct s {
	double sums[MAX_SIZE];
	int* ranges;
	double* vector;
} data_t;

int
get_range(int mem_id, int n);

void
on_usr1(int signal);

void
print_double_array(double *t, int size);

double
sum_vector(double *t);

void
assign_usr1_handler();

int* 
create_ranges_array(int n, int no_of_children);

#endif
