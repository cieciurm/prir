#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <time.h>

#include "functions.h"

int flag = 0;

double*
get_vector_from_file(char* filename) {
	double *vector;
	char buffor[BUFFOR_SIZE+1];
	int n, vector_size;
	FILE *f;

	f = fopen(filename, "r");

	if (f == NULL) {
		printf("Blad otwierania pliku\n");
		exit(16);
	}

	fgets(buffor, BUFFOR_SIZE, f);
	vector_size = atoi(buffor);

	vector = malloc((vector_size+1) * sizeof(double));

	if (vector == NULL) {
		printf("Blad alokacji pamieci\n");
		exit(15);
	}

	vector[0] = vector_size;

	n = 1;
	while (fgets(buffor, BUFFOR_SIZE, f) != NULL) {
		vector[n] = atof(buffor);
		n++;
	}

	fclose(f);
	return vector;
}

double
calc_part_sum(int mem_id, double* vector, int current_child) {
	int begin, end, i;
	double sum;

	sum = 0.0;

	/* zwraca koniec przedzialu - prawostronnie otwarty */
	end = get_range(mem_id, current_child);

	if (current_child == 0) { /* pierwszy element */
		begin = 1;
	}
	else {
		begin = get_range(mem_id, current_child - 1) + 1;
	}

	for (i = begin; i <= end; i++)
		sum += vector[i];

	/*printf("[%d dziecko] begin=%d, end=%d, rozmiar=%d\n", current_child, begin, end, (end-begin+1));*/

	return sum;
} 


int
main(int argc, char** argv) {
	key_t key;
	int mem_id, i, n_children, finished_process, vector_size;
	int *pids;
	double total_sum;
	double *vector;
	data_t *adr, *adr2, *d;
	pid_t pid;
	char* filename;

	if (argc > 2) {
		n_children = atoi(argv[1]);
		filename = argv[2];
	} else {
		exit(-1);
	}

	key = ftok(argv[0], ID);

	mem_id = shmget(key, sizeof(data_t), IPC_CREAT|0666);
	if (mem_id == -1) {
		printf("Blad shmget!\n");
		exit(1);
	}

	adr = shmat(mem_id, 0, 0);
	if (adr == (data_t *)-1) {
		printf("Blad shmat!\n");
		exit(2);
	}

	vector = get_vector_from_file(filename);
	vector_size = vector[0];

	d = malloc(sizeof(data_t));
	d->ranges = create_ranges_array(vector_size, n_children);
	d->vector = vector;

	memcpy(adr, d, sizeof(data_t));

	/* przypinamy obsluge sygnalu */
	assign_usr1_handler();

	pids = malloc(n_children * sizeof(int));

	for (i = 0; i < n_children; i++) {
		pid = fork();

		if (pid == 0) { /* proces potomny */
			adr2 = shmat(mem_id, 0, 0);

			if (adr2 == (data_t *)-1) {
				printf("Blad shmat!\n");
				exit(2);
			}

			/* czekam na sygnał */
			while (flag == 0)
				pause();
			
			adr2->sums[i] = calc_part_sum(mem_id, adr2->vector, i);

			return 0;
		} else if (pid == -1) {
			printf("Blad fork!\n");
			exit(4);
		} else {
			pids[i] = pid;
		}

	}

	/*sleep(1);*/

	for (i = 0; i < n_children; i++) {
		kill(pids[i], SIGUSR1);
	}

	/* czekamy az skoncza sie procesy potomne */
	for (i = 0; i < n_children; i++) {
		finished_process = wait(0);
		if (finished_process == -1)
			exit(5);
	}

	adr = shmat(mem_id, 0, 0);
	if (adr == (data_t *)-1) {
		printf("Blad shmat!\n");
		exit(2);
	}

	/*wypisz sumy czastkowe*/
	/*for (i = 0; i < n_children; i++)*/
	/*printf("[%g]", adr->sums[i]);*/
	/*printf("\n");*/

	total_sum = 0;

	for (i = 0; i < n_children; i++)
		total_sum += adr->sums[i];

	/*printf("Suma wektora=%g\n", total_sum);*/
	printf("Suma=%g\nN=%d\nPlik=%s\nDzieci=%d\n", total_sum, vector_size, filename, n_children);

	if (shmctl(mem_id, IPC_RMID, NULL) != 0) {
		printf("Blad przy zwalnianiu pamieci!\n");
		int error = errno;
		printf("errno: %d\n", error);
		printf("error: %s\n", strerror(error));
		exit(-2);
	}

	free(d);

	return 0;
}
