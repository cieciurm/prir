#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <time.h>

#include "functions.h"

int flag = 0;

double*
get_part_vector(char* filename, int mem_id, int current_child) {
	int begin, end;
	double *part_vector;
	char buffor[BUFFOR_SIZE+1];
	int n, vector_size, current_line;
	FILE *f;

	/* zwraca koniec przedzialu - prawostronnie otwarty */
	end = get_range(mem_id, current_child) - 1;

	if (current_child == 0) { /* pierwszy element */
		begin = 0;
	}
	else {
		begin = get_range(mem_id, current_child - 1);
	}

	vector_size = end - begin + 1;

	f = fopen(filename, "r");
	
	if (f == NULL) {
		printf("Blad podczas otwierania pliku\n");
		exit(7);
	}

	part_vector = malloc(sizeof(double) * vector_size + 1);
	part_vector[0] = vector_size;

	/* linia z liczba elementow */
	fgets(buffor, BUFFOR_SIZE, f);
	/* linia na ktorej jestesmy, liczac od 0 */
	current_line = 0;
	n = 1;

	while (fgets(buffor, BUFFOR_SIZE, f) != NULL) {
		if (current_line >= begin && current_line <= end) {
			part_vector[n] = atof(buffor);
			n++;
		}
		current_line++;
		if (current_line > end)
			break;
	}

	fclose(f);
	return part_vector;
} 


int
main(int argc, char** argv) {
	key_t key;
	int mem_id, i, n_children, finished_process, vector_size;
	int *pids;
	double total_sum;
	double *vector_part;
	data_t *adr, *adr2, *d;
	pid_t pid;
	char* filename;
	FILE *f;
	char buffor[BUFFOR_SIZE];

	if (argc > 2) {
		n_children = atoi(argv[1]);
		filename = argv[2];
	} else {
		exit(-1);
	}

	f = fopen(filename, "r");
	fgets(buffor, BUFFOR_SIZE, f);
	fclose(f);

	vector_size = atoi(buffor);

	key = ftok(argv[0], ID);

	mem_id = shmget(key, sizeof(data_t), IPC_CREAT|0666);
	if (mem_id == -1) {
		printf("Blad shmget!\n");
		exit(1);
	}

	/*printf("|---------------\n");*/
	/*printf("| mem_id = %d\n", mem_id);*/
	/*printf("|---------------\n\n");*/

	adr = shmat(mem_id, 0, 0);
	if (adr == (data_t *)-1) {
		printf("Blad shmat!\n");
		exit(2);
	}

	d = malloc(sizeof(data_t));
	d->ranges = create_ranges_array(vector_size, n_children);

	/*for (i = 0; i < n_children; i++) */
	/*printf("[%d]", d->ranges[i]);*/
	/*printf("\n");*/

	memcpy(adr, d, sizeof(data_t));

	/* przypinamy obsluge sygnalu */
	assign_usr1_handler();

	pids = malloc(n_children * sizeof(int));

	for (i = 0; i < n_children; i++) {
		pid = fork();

		if (pid == 0) { /* proces potomny */
			adr2 = shmat(mem_id, 0, 0);

			if (adr2 == (data_t *)-1) {
				printf("Blad shmat!\n");
				exit(2);
			}

			/* czekam na sygnał */
			while (flag == 0)
				pause();

			vector_part = get_part_vector(filename, mem_id, i);
			/*print_double_array(vector_part, vector_part[0]);*/
			/*printf("Suma wektorka = %g\n", sum_vector(vector_part));*/
			adr2->sums[i] = sum_vector(vector_part);

			return 0;
		} else if (pid == -1) {
			printf("Blad fork!\n");
			exit(4);
		} else {
			pids[i] = pid;
		}
	}

	/*sleep(1);*/

	for (i = 0; i < n_children; i++) {
		/*sleep(1);*/
		kill(pids[i], SIGUSR1);
	}
	/*printf("[%d]", pids[i]);*/
	/*printf("\n");*/

	/* czekamy az skoncza sie procesy potomne */
	for (i = 0; i < n_children; i++) {
		finished_process = wait(0);
		if (finished_process == -1)
			exit(5);
		/*printf("* Proces ktory sie zakonczyl PID=%d\n", finished_process);*/
	}

	adr = shmat(mem_id, 0, 0);
	if (adr == (data_t *)-1) {
		printf("Blad shmat!\n");
		exit(2);
	}

	/* wypisz sumy czastkowe */
	/*for (i = 0; i < n_children; i++)*/
	/*printf("[%g]", adr->sums[i]);*/
	/*printf("\n");*/

	total_sum = 0;

	for (i = 0; i < n_children; i++)
		total_sum += adr->sums[i];

	/*printf("Suma wektora=%g\n", total_sum);*/
	printf("Suma=%g\nN=%d\nPlik=%s\nDzieci=%d\n", total_sum, vector_size, filename, n_children);

	if (shmctl(mem_id, IPC_RMID, NULL) != 0) {
		printf("Blad przy zwalnianiu pamieci!\n");
		int error = errno;
		printf("errno: %d\n", error);
		printf("error: %s\n", strerror(error));
		exit(-2);
	}

	free(d);

	return 0;
}
